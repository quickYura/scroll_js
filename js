window.onload = () => {
    (function scroll() {
        const wrap_dot_link = document.querySelector('.dot_link');
        const dot_link = document.querySelectorAll('.link');
        const section = document.querySelectorAll(".section");
        let sections_arr = [];
        dot_link[0].classList.add('active_dot_link');
        let block_height_content = 0;
        let dot_link_height = wrap_dot_link.clientHeight;
        let isSection_last = true;
        let timClear;

//---shutdown dot_link if greater than the width of the screen
        const controlTheDisplayOfButtons = () => {
            dot_link_height + 200 > screen.height ?
                wrap_dot_link.style.display = 'none' :
                wrap_dot_link.style.display = 'block';
        };
        controlTheDisplayOfButtons();
//----shutdown dot_link if greater than the width of the screen end
        window.addEventListener("resize", () => {
            sections_arr.length = 0;
            block_height_content = 0;
            controlTheDisplayOfButtons();
            creationHeightDataEachElement()


        });
//--- Event Listener dot link

        dot_link.forEach((el, index) => {
            el.addEventListener('click', (e) => {
                changeLinkStyle(el, index);
                ScrollToResolver(section[index]);
                e.preventDefault();
            })
        });

        const changeLinkStyle = (el) => {
            dot_link.forEach(i => i.classList.remove('active_dot_link'));
            el.classList.add('active_dot_link');
        };
 //---    Event Listener dot link end

//---------smooth scrolling to anchor-----

        function ScrollToResolver(elem) {
            let jump = elem.getBoundingClientRect().top -100;
            document.body.scrollTop += jump * 0.2;
            document.documentElement.scrollTop += jump * 0.2;
            if (!elem.lastjump || elem.lastjump > Math.abs(jump)) {
                elem.lastjump = Math.abs(jump);
                if( elem.lastjump   < 1 )return;
                elem.lastjump --;
                timClear = setTimeout(() => {
                    ScrollToResolver(elem);
                }, 20);
            } else {
                elem.lastjump = null;
                clearTimeout(timClear)

            }
        }
//---------smooth scrolling to anchor-----end--
//--------lights up when scrolling dot link ------------
        let scrollHeight;
        let clientHeight;
        let scroll;
        let scrolling_percentage_calc = 0;

        function calculateSizeAndScrollDevice(el) {
            scrollHeight = Math.max(
                document.body.scrollHeight, document.documentElement.scrollHeight,
                document.body.offsetHeight, document.documentElement.offsetHeight,
                document.body.clientHeight, document.documentElement.clientHeight
            );
            clientHeight = document.documentElement.clientHeight;
            scroll = (scrollHeight - clientHeight);
            scrolling_percentage_calc = (scroll / scrollHeight) * 100;
            let intermediate_result = (el / scroll) * scrolling_percentage_calc;
            return (intermediate_result * scroll) / 100;

        }

        function creationHeightDataEachElement() {
            section.forEach((e, index) => {
                block_height_content += calculateSizeAndScrollDevice(e.getBoundingClientRect().height);
                isSection_last = true;
                if (index === section.length - 1) isSection_last = false;
                sections_arr.push([block_height_content, e.id, isSection_last, index]);
            });

        }

        creationHeightDataEachElement();

        window.onscroll = function () {
            let scrollPosition = window.pageYOffset || document.documentElement.scrollTop || document.body.scrollTop;
            sections_arr.map((on_el, index) => {
                if ((scrollPosition >= sections_arr[index - 1 < 0 ? 0 : index - 1][0] && on_el[0] >= scrollPosition) && on_el[2]) {
                    styleManagementDotLink(on_el[1]);

                } else {
                    if (scrollPosition < 50) styleManagementDotLink(sections_arr[0][1]);
                }

                if (scrollHeight - scrollPosition === clientHeight) styleManagementDotLink(on_el[1]);


            })
        };
        const styleManagementDotLink = (on_el) => {
            document.querySelector('.active_dot_link').setAttribute('class', '');
            document.querySelector(`a[href*='${on_el}']`).setAttribute('class', 'active_dot_link');

        }
    })();

};